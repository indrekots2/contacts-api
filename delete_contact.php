<?php

require_once("Contact.php");
require_once("functions.php");

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $id = intval($_GET["id"]);
    deleteContactById($id);
    http_response_code(204);
} else {
    http_response_code(405);
}