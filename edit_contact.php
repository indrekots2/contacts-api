<?php

require_once("Contact.php");
require_once("functions.php");

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $json = file_get_contents("php://input");
    $contactData = json_decode($json, true);
    $id = $_GET["id"];

    $contact = Contact::fromAssocArray($contactData);
    $contact->id = $id;
    $errors = $contact->validate();

    if (count($errors) > 0) {
        http_response_code(400);
        printJson(["errors" => $errors]);
    } else {
        updateContact($contact);
        printJson($contact);
    }
} else {
    http_response_code(405);
}