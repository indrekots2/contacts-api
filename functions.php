<?php

require_once("Contact.php");

function createConnection() {
    $user = 'root';
    $password = 'password';

    $connection = new PDO('mysql:dbname=contacts;host=127.0.0.1', $user, $password);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    return $connection;
}

function findAllContacts() {
    $connection = createConnection();

    $statement = $connection->prepare("select id, name, email, phone from contacts");
    $statement->execute();

    $contacts = [];

    foreach ($statement as $row) {
        $contact = new Contact($row['name'], $row['email'], $row['phone'], intval($row['id']));
        $contacts[] = $contact;
    }

    return $contacts;
}

function findContactById($id) {
    $connection = createConnection();

    $statement = $connection->prepare("select id, name, email, phone from contacts where id = :id");
    $statement->bindValue(":id", $id);
    $statement->execute();

    $contact = null;
    foreach ($statement as $row) {
        $contact = new Contact($row['name'], $row['email'], $row['phone'], $row['id']);
    }

    return $contact;
}

function saveContact($contact) {
    $connection = createConnection();

    $statement = $connection->prepare("insert into contacts (name, email, phone)
            values(:name, :email, :phone)");

    $statement->bindValue(':name', $contact->name);
    $statement->bindValue(':email', $contact->email);
    $statement->bindValue(':phone', $contact->phone);
    $statement->execute();

    $id = $connection->lastInsertId();
    $contact->id = $id;
    return $contact;
}

function updateContact($contact) {
    $connection = createConnection();

    $statement = $connection->prepare("update contacts
                    set name = :name, email = :email, phone = :phone
                    where id = :id");

    $statement->bindValue(":name", $contact->name);
    $statement->bindValue(":email", $contact->email);
    $statement->bindValue(":phone", $contact->phone);
    $statement->bindValue(":id", $contact->id);
    $statement->execute();
}

function deleteContactById($id) {
    $connection = createConnection();

    $statement = $connection->prepare("delete from contacts where id = :id");
    $statement->bindValue(":id", $id);
    $statement->execute();
}

function printJson($obj) {
    header("Content-Type: application/json");
    print json_encode($obj);
}
