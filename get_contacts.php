<?php

require_once("functions.php");
require_once("Contact.php");

if (isset($_GET['id'])) {
    $contact = findContactById(intval($_GET['id']));

    if ($contact == null) {
        http_response_code(404);
    } else {
        printJson($contact);
    }
} else {
    $contacts = findAllContacts();
    printJson($contacts);
}


